/**
 *       @file  simple_exc.h
 *      @brief SimpleEXC class for Tutorial 2
 *
 * This defines the SimpleEXC class for supporting Tutorial 2.
 *
 *     @author  Giuseppe Massari (jumanix), joe.massanga@gmail.com
 *
 *   @internal
 *     Created  07/18/2011
 *    Revision  $Id: doxygen.templates,v 1.3 2010/07/06 09:20:12 mehner Exp $
 *    Compiler  gcc/g++
 *     Company  Politecnico di Milano
 *   Copyright  Copyright (c) 2011, Giuseppe Massari
 *
 * This source code is released for free distribution under the terms of the
 * GNU General Public License as published by the Free Software Foundation.
 * ============================================================================
 */

#ifndef BBQUE_TUTORIAL_AEM_
#define BBQUE_TUTORIAL_AEM_

#include <list>
#include <thread>

#include <bbque/bbque_exc.h>

using namespace std;
using bbque::rtlib::BbqueEXC;

class SimpleEXC: public BbqueEXC {

public:

	SimpleEXC(std::string const & name,
			std::string const & recipe,
			RTLIB_Services_t *rtlib):
		BbqueEXC(name, recipe, rtlib) {
	};

	~SimpleEXC() { }

	RTLIB_ExitCode_t onSetup();

	RTLIB_ExitCode_t onRun();

	RTLIB_ExitCode_t onConfigure(uint8_t awm_id);

	RTLIB_ExitCode_t onSuspend();

	RTLIB_ExitCode_t onMonitor();

private:

	list<thread> thrds;

	uint8_t num_thrds;

};

#endif // BBQUE_TUTORIAL_AEM_

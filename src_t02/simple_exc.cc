/**
 *       @file  simple_exc.cc
 *      @brief  Implementation for the SimplEXC class extending Bbque
 *
 * This implements the class used in Tutorial 2.
 *
 *     @author  Giuseppe Massari (jumanix), joe.massanga@gmail.com
 *
 *   @internal
 *     Created  07/18/2011
 *    Revision  $Id: doxygen.templates,v 1.3 2010/07/06 09:20:12 mehner Exp $
 *    Compiler  gcc/g++
 *     Company  Politecnico di Milano
 *   Copyright  Copyright (c) 2011, Giuseppe Massari
 *
 * This source code is released for free distribution under the terms of the
 * GNU General Public License as published by the Free Software Foundation.
 * ============================================================================
 */

#include "simple_exc.h"

#include <algorithm>
#include <iostream>


/****************************************************************************
 *                                                                          *
 *              Functions....
 *                                                                          *
 ***************************************************************************/

void join_thread(thread & t) {
	t.join();
}


//
// The task to run
//
void HelloWorld() {
	cout << "\nHello eaters from Barbeque World!" << endl;
	cout << "Have you tasted the Angus yet ? " << endl << endl;
	sleep(2);
}

/****************************************************************************
 *                                                                          *
 *                Methods inherited from base class BbqueEXC                *
 *                                                                          *
 ****************************************************************************/


RTLIB_ExitCode_t SimpleEXC::onSetup() {

	/** Here must be placed initialization code */

	return RTLIB_OK;
}


RTLIB_ExitCode_t SimpleEXC::onRun() {
	static uint16_t cycles = 0;
	cout << "\n[Running cycle " << ++cycles << "]"<< endl;

	// Spawn threads
	for (int i = 0; i < num_thrds; ++i) {
		thrds.push_back(thread(HelloWorld));
	}

	// Join threads
	for_each(thrds.begin(), thrds.end(), join_thread);
	thrds.clear();

	// Exit condition
	if (cycles == 5)
		return RTLIB_EXC_WORKLOAD_NONE;

	return RTLIB_OK;
}


RTLIB_ExitCode_t SimpleEXC::onConfigure(uint8_t awm_id) {

	// In the "recipe" associated to the task just 3 working modes are
	// defined (IDs from 0 to 2). The lower the ID the higher its value and
	// thus the resources granted.

	switch (awm_id) {

	case 2:
		num_thrds = 3;
		break;

	case 1:
		num_thrds = 2;
		break;

	case 0:
		num_thrds = 1;
		break;

	default:
		num_thrds = 1;
		break;
	}

	return RTLIB_OK;
}


RTLIB_ExitCode_t SimpleEXC::onSuspend() {

	/** Do something here if EXC has been forced to release resources */

	sleep (5);
	return RTLIB_OK;
}


RTLIB_ExitCode_t SimpleEXC::onMonitor() {

	/** Do something here to monitor the QoS */

	return RTLIB_OK;
}







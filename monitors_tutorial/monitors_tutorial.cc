#include "utility.h"
#include <vector>
#include <iostream>
#include <pthread.h>
#include <bbque/monitors/throughput_monitor.h>
#include <bbque/monitors/time_monitor.h>
#include <bbque/bbque_exc.h>

using namespace std;
using bbque::rtlib::BbqueEXC;
// Tutorial messages file
ifstream msgs_file;


const char app_name[] = "MonitorTutorial";
const char exc_name[] = "monitor_tutorial";


int main(int argc, char *argv[]) {
	
	const char *messages = "./monitors_tutorial.txt";
	TimeMonitor timeMonitor,timer2;
	ThroughputMonitor throughputMonitor;
	uint16_t idTime,idTh;
	bool goalAchieved=0;
	
	if (argc > 1) {
		messages = argv[1];
	}

	clearScreen();
	msgs_file.open(messages);
	if (!msgs_file.is_open()) {
		std::cout << "Open tutorial messages file ["
			<< messages << "] FAILED" << std::endl
			<< "Usage: ./MonitorTutorial <messages_file>\n"
			<< std::endl;
		return -1;
	}

	TTR_MESSAGE("_SPLASH_BANNER", COLOR_LRED, STOP_NONE);
	TTR_MESSAGE("_SPLASH_MSG", COLOR_GRAY, STOP_PAUSE);
	clearScreen();
	
	TTR_MESSAGE("_INTRO_CLASS", COLOR_GRAY, STOP_PAUSE);
	clearScreen();
	
	
	TTR_MESSAGE("_MTIME_INTRODUCTION", COLOR_GRAY, STOP_NONE);
	TTR_MESSAGE("_MTIME_INIT", COLOR_GRAY, STOP_PAUSE);
	TTR_MESSAGE("_MTIME_INIT_CODE", COLOR_WHITE, STOP_PAUSE);
	TTR_MESSAGE("_MTIME_INIT_DETAIL", COLOR_GRAY, STOP_PAUSE);
	clearScreen();
	
	TTR_MESSAGE("_MTIME_START_STOP", COLOR_GRAY, STOP_PAUSE);
	TTR_MESSAGE("_MTIME_START_STOP_CODE", COLOR_WHITE, STOP_PAUSE);
	clearScreen();
	
	TTR_MESSAGE("_MTIME_CHECK", COLOR_GRAY, STOP_PAUSE);
	TTR_MESSAGE("_MTIME_CHECK_CODE", COLOR_WHITE, STOP_PAUSE);
	clearScreen();
	
	TTR_MESSAGE("_MTIME_EXECUTION", COLOR_GRAY, STOP_PAUSE);
	TTR_MESSAGE("_MTIME_EXECUTION_CODE", COLOR_WHITE, STOP_SHOW);
	
	std::cout.setf(std::ios::boolalpha);
	vector<float> nap;
	idTime = timeMonitor.newGoal(DataFunction::Average,
								 ComparisonFunction::LessOrEqual,1000,3);
	for(int i=0;i<3;i++)
	{
		timeMonitor.start(idTime);
		usleep(900000);
		timeMonitor.stop(idTime);
	}
	
	goalAchieved =timeMonitor.checkGoal(idTime,nap);
	std::cout << "Timer checkGoal " << goalAchieved<<std::endl;
	std::cout << "Nap " <<nap.at(0) << std::endl;
	double timeTemp = timeMonitor.getAverage(idTime);
	std::cout << "Average " << timeTemp << std::endl;
	
	TTR_MESSAGE("_MTIME_RESULT", COLOR_GRAY, STOP_PAUSE);
	clearScreen();
	
	TTR_MESSAGE("_MTIME_ADDITIONAL", COLOR_GRAY, STOP_PAUSE);
	TTR_MESSAGE("_MTIME_ADDITIONAL_CODE", COLOR_WHITE, STOP_PAUSE);
	clearScreen();

	TTR_MESSAGE("_BASIC_MTIME", COLOR_GRAY, STOP_PAUSE);
	TTR_MESSAGE("_BASIC_MTIME_CODE", COLOR_WHITE, STOP_PAUSE);
	TTR_MESSAGE("_TRY", COLOR_GRAY, STOP_NONE);
	TTR_MESSAGE("_BASIC_MTIME_EXECODE", COLOR_WHITE, STOP_SHOW);
	
	timeMonitor.start();
	sleep(1);
	timeMonitor.stop();
	std::cout << "Timer basic (s) " << timeMonitor.getElapsedTime() 
									<< std::endl;
	std::cout << "Timer basic (ms)" << timeMonitor.getElapsedTimeMs() 
									<< std::endl;
	
	TTR_MESSAGE("_BASIC_MTIME_EXECOMMENT", COLOR_GRAY, STOP_PAUSE);
	clearScreen();
	TTR_MESSAGE("_MTIME_ADVANCED", COLOR_GRAY, STOP_PAUSE);
	TTR_MESSAGE("_MTIME_ADVANCED_CODE", COLOR_WHITE, STOP_PAUSE);
	clearScreen();
	
	TTR_MESSAGE("_MTHROUGHPUT_DIFFERENCE", COLOR_GRAY, STOP_PAUSE);
	TTR_MESSAGE("_MTHROUGHPUT_DIFFERENCE_CODE", COLOR_WHITE, STOP_PAUSE);
	TTR_MESSAGE("_TRY", COLOR_GRAY, STOP_NONE);
	TTR_MESSAGE("_MTHROUGHPUT_DIFFERENCE_EXECODE", COLOR_WHITE, STOP_SHOW);
	
	nap.clear();
	idTh = throughputMonitor.newGoal(DataFunction::Average,
									 ComparisonFunction::GreaterOrEqual, 
									 1000, 5);
	throughputMonitor.start(idTh);
	sleep(1);
	throughputMonitor.stop(idTh,1100);
	std::cout << "throughput checkGoal " 
			  << throughputMonitor.checkGoal(idTh,nap) << std::endl;
	std::cout << "gap " << nap.at(0) << std::endl;
	
	TTR_MESSAGE("_MTHROUGHPUT_DIFFERENCE_EXECOMMENT", COLOR_GRAY, STOP_PAUSE);
	clearScreen();
	
	TTR_MESSAGE("_MMEMORY_DIFFERENCE", COLOR_GRAY, STOP_PAUSE);
	TTR_MESSAGE("_MMEMORY_DIFFERENCE_CODE", COLOR_WHITE, STOP_PAUSE);
	clearScreen();

	TTR_MESSAGE("_BYE", COLOR_LRED, STOP_NONE);

	return EXIT_SUCCESS;
}

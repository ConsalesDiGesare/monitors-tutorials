

#----- Check for the required monitors library
find_package(BbqMonitors REQUIRED)

#----- Add compilation dependencies
include_directories(${BBQUE_RTLIB_INCLUDE_DIR})

#----- Add "monitors_tutorial" target application
set(MONITORS_TUTORIAL_SRC monitors_tutorial)
add_executable(monitors_tutorial ${MONITORS_TUTORIAL_SRC})


#----- Linking dependencies
target_link_libraries(
	monitors_tutorial
	${BBQUE_MONITORS_LIBRARY}
	-lrt
)

# Use link path ad RPATH
set_property(TARGET monitors_tutorial PROPERTY
	INSTALL_RPATH_USE_LINK_PATH TRUE)

#----- Install the Tutorial files
install (TARGETS monitors_tutorial RUNTIME
	DESTINATION ${RTLIB_TUTORIAL_PATH_BINS}
	COMPONENT monitors_tutorial)
install (FILES "./monitors_tutorial.txt"
	DESTINATION ${RTLIB_TUTORIAL_PATH_BINS}
	COMPONENT monitors_tutorial)

